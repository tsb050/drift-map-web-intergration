"""
Utility Routines
================
:Author: Clyde Clements
:Created: 2017-08-04
"""

from collections import namedtuple
import logging
import os
from os import path
import shutil

import numpy as np
import xarray as xr

# For backward compatibility.
from .cli import initialize_logging, log_level, shutdown_logging  # noqa: F401

# Global variables.
logger = logging.getLogger('drifter')

LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

# Default variable names.
defaults = {
    'xwatervel': {'long_name': 'X water velocity', 'nc_name': 'vozocrtx'},
    'ywatervel': {'long_name': 'Y water velocity', 'nc_name': 'vomecrty'},
    'zwatervel': {'long_name': 'Z water velocity', 'nc_name': 'vovecrtz'},
    'temperature': {'long_name': 'temperature', 'nc_name': 'votemper'},
    'salinity': {'long_name': 'salinity', 'nc_name': 'vosaline'},
    'density': {'long_name': 'density', 'nc_name': 'density'}
}


def interpolate_track(obs, mod, start=0, end=None):
    """Linearly interpolate modelled positions to times when observed
    positions were recorded.

    Parameters
    ----------
    obs : xarray.Dataset
        Observed trajectory data. Required elements:
        * `time`: DataArray containing timestamps
        * `lon`: DataArray containing longitudes
        * `lat`: DataArray containing latitudes
    mod : xarray.Dataset
        Modelled trajectory data. It must contain the same element types as
        `obs`.

    Returns
    -------
    interp_track : xarray.Dataset
        Interpolated track with a `time` coordinate and data variables `lon`
        and `lat`.
    """
    if end is None:
        oTime = obs['time'][start:]
    else:
        oTime = obs['time'][start:end]

    lat = np.interp(
        oTime.astype('float64').values, mod['time'].astype('float64').values,
        mod['lat'][0].values)
    lon = np.interp(
        oTime.astype('float64').values, mod['time'].astype('float64').values,
        mod['lon'][0].values)
    interp_track = xr.Dataset(
        data_vars={'lon': ('time', lon), 'lat': ('time', lat)},
        coords={'time': oTime})
    return interp_track


def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


def colocate_model_to_obs(model_data, obs, output_dataset=None):
    """Colocate model variables in space and time  to observation locations."""
    # FIXME: Need to implement method.
    return


drift_model_symbols = {'Ariane': 'ar', 'MLDP': 'ml', 'OpenDrift': 'od'}


def set_run_name(ocean_model, drift_model, start, duration):
    """Construct name for drift simulation from given parameters.

    Parameters
    ----------
    ocean_model : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    drift_model : str
        Name of model for drift simulation (e.g. Ariane, MLDP or OpenDrift).
    start : datetime.datetime
        Start date of drift trajectory calculation.
    duration : datetime.timedelta
        Duration of drift trajectory calculation.

    Returns
    -------
    run_name : str
        Name for drift simulation.

    Details
    -------
    The name will have the following format::

       omod_dmod_YYYYmmddHH[MM]_P[<num_days>D][T<num_hours>H]

    The components of the name are:

    - ``omod``: The name of the ocean model converted to lowercase and spaces
      replaced by dashes.

    - ``dmod``: A two-letter code for the drift model, with ``ar`` representing
      Ariane, ``ml` for MLPD and ``od`` for OpenDrift.

    - ``YYYYmmddHH[MM]``: The start date of the drift trajectory. Note start
      dates will normally begin on the hour in which case the 0 minute
      component will not be present. Start dates will not include a component
      for seconds as such a resolution is not expected to be needed.

    - ``P<num_days>D<num_hours>H``: The remaining part of the name corresponds
      to the duration of drift and is represented in the ISO 8601 format for
      durations with the assumption that drift durations will only be specified
      to the nearest hour. If non-zero, the number of hours will be represented
      in a zero-padded field of width 2. The number of days will not be zero
      padded.

    Examples
    --------
    For a start date of 2017-04-12, drift duration of 48 hours and using GIOPS
    model data with Ariane, the run name will be "giops_ar_2017041200_P2D".
    Note that a start date without a time component corresponds to midnight.

    For a start date of 2017-04-12T16:30 and drift duration of 12 hours with
    Salish Sea model data and Ariane particle model, the run name will be
    "salish-sea_ar_201704121630_PT12H".
    """
    run_name = ocean_model.lower()
    run_name = run_name.replace(' ', '-')
    try:
        drift_model_symbol = drift_model_symbols[drift_model]
    except KeyError:
        valid_models = ', '.join(drift_model_symbols.keys())
        msg = 'Unknown drift model "{0}"; valid choices are {1}'.format(
            drift_model, valid_models)
        raise ValueError(msg)
    run_name += '_' + drift_model_symbol + '_'

    if start.second != 0:
        logger.warn(('Non-zero second component for start date will not be '
                     'represented in the run name.'))
    if start.minute == 0:
        run_name += start.strftime('%Y%m%d%H')
    else:
        run_name += start.strftime('%Y%m%d%H%M')

    minutes, seconds = divmod(int(duration.total_seconds()), 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)
    date = ''
    time = ''
    if days > 0:
        date = '{}D'.format(days)
    if hours > 0:
        time += '{:02}H'.format(hours)
    if minutes > 0:
        # time += '{:02}M'.format(minutes)
        logger.warn(('Non-zero minute component for drift duration will not '
                     'be represented in the run name.'))
    if seconds > 0:
        # time += '{:02}S'.format(seconds)
        logger.warn(('Non-zero second component for drift duration will not '
                     'be represented in the run name.'))
    if time:
        time = 'T' + time
    run_name += '_P' + date + time

    return run_name



def find_subset_indices(lat_min, lat_max, lon_min, lon_max, lats, lons):
    """Finds indices in the arrays lats and lons containing the specified box.
       This function is called in the plot_bathemetry module.
    
    Parameters
    ----------
    lat_min : float
    lat_max : float
    lon_min : float
    lon_max : floatimport matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.cbook import mplDeprecation
from mpl_toolkits.basemap import Basemap
    lats : numpy.ndarray
    lons : numpy.ndarray

    Returns
    -------
    LatLonBoundingBox
        The integer indices specified as a dictionary with keys 'lat_min',
        'lat_max', 'lon_min', and 'lon_max'.
    """
    distances1 = []
    distances2 = []
    index = 1

    for point in lats:
        s1 = lat_max - point
        s2 = lat_min - point
        distances1.append((s1 * s1, point, index))
        distances2.append((s2 * s2, point, index - 1))
        index = index + 1

    distances1.sort()
    distances2.sort()
    ilat_max = distances1[0][2]
    ilat_min = distances2[0][2]

    distances1 = []
    distances2 = []
    index = 1

    for point in lons:
        s1 = lon_max - point
        s2 = lon_min - point
        distances1.append((s1 * s1, point, index))
        distances2.append((s2 * s2, point, index - 1))
        index = index + 1

    distances1.sort()
    distances2.sort()
    ilon_max = distances1[0][2]
    ilon_min = distances2[0][2]

    return LatLonBoundingBox(lon_min=ilon_min, lat_min=ilat_min,
                             lon_max=ilon_max, lat_max=ilat_max)
