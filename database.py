import glob
import sqlite3
import os
from sqlite3 import Error
import xarray as xr
 
 
def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
        return conn
    except Error as e:
        print(e)
 
    return conn
 
 
def create_table(conn, create_table_sql):
    """ create a table from the create_table_sql statement
    :param conn: Connection object
    :param create_table_sql: a CREATE TABLE statement
    :return:
    """
    try:
        c = conn.cursor()
        c.execute(create_table_sql)
    except Error as e:
        print(e)
 
def create_project(conn, paths):
    """
    Create a new project into the projects table
    :param conn:
    :param paths:
    :return: project id
    """
    sql = ''' INSERT INTO paths(ocean_model, start_date, file_path)
              VALUES(?,?,?) '''
    cur = conn.cursor()
    cur.execute(sql, paths)

def select_all_tasks(conn):
    """
    Query all rows in the tasks table
    :param conn: the Connection object
    :return:
    """
    cur = conn.cursor()
    cur.execute("SELECT * FROM paths")
 
    rows = cur.fetchall()
    for row in rows:
        print(row)

def main():
    database = '{}/filepaths.db'.format(os.getcwd())
 
    sql_create_projects_table = """ CREATE TABLE IF NOT EXISTS paths (
                                        ocean_model text,
                                        start_date text,
                                        file_path text
                                    ); """
 
    # create a database connection
    conn = create_connection(database)

    with conn:
        files = glob.glob(os.path.join('model_file', '*nc'))
        paths = ('CIOPSE', '2016-06-10', '/home/samuel/map_integrate/model_files/ciops-e_ar_2016061000_P10D.nc')
        create_project(conn, paths)
        #select_all_tasks(conn)
 
if __name__ == '__main__':
    main()