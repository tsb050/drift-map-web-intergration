
from collections import namedtuple
from collections import OrderedDict
import glob
import json
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from matplotlib.pylab import cm
from mpl_toolkits.basemap import Basemap
import numpy as np
import os
from os.path import join as joinpath
import re
import requests
import shutil
import xarray as xr
from urllib.parse import urlencode

from flask import Flask, render_template, request, jsonify
from flask_mysqldb import MySQL
app = Flask(__name__)


@app.route('/', methods=['GET', 'POST'])
def front():
    image=None
    ocean_url = '{}{}'.format(request.base_url, 'ocean_models')
    oc_res = requests.get(ocean_url)
    oc_data = oc_res.json()
    return render_template('index2.html', user_image=image, 
                            model_metadata=oc_data, output_file=None, settings=None)
    
    # Sent API request to back-end to retrieve drifter output information available

@app.route('/index', methods=['GET', 'POST'])
def index():
    """
    Render image to the html page. Using the GET method and 
    user information.

    return: HTML with rendered image
    """
    image=None
    ocean_url = '{}{}'.format(request.base_url.split('index')[0], 'ocean_models')
    oc_res = requests.get(ocean_url)
    oc_data = oc_res.json()
    if request.method == "GET":
        details = request.args
        bbox = details['BoundingBox']
        duration = details.get('duration')
        model = details.get('model')
        date = details['Date']
        settings_list = [model, bbox, date, duration]
        for keys in oc_data:
            if(model == keys['model']):
                new_date = date.split("-")
                file_date = '{}{}{}'.format(new_date[0],new_date[1], new_date[2])
                check_png = []
                #glob.glob(os.path.join('static','*{}*_*{}*'.format(file_date, duration)))
                model_output_file, message = choose_file(file_date, model)
                fetch_file = glob.glob(os.path.join('model_files/{}'.format(model), '*{}*'.format(file_date)))
                file_info = [model, new_date[0], fetch_file[0].split("/")[2]]
                if len(check_png)!=0:
                    image = check_png[0]
                elif message is None:
                    image = plot_ocean_data(model_output_file, bbox, duration, None)
                else:
                    return render_template('error.html', message=message)
    return render_template('index2.html', user_image=image, model_metadata=oc_data, 
                            output_file=file_info, settings=settings_list)

def choose_file(date, model):
    """
    Select drifter output file based on user-specified date
    This sends an API request to the file container and returns
    the file for the specified date.

    return: Drifter output file, message (in case file is not available
                                          for specified date)
        
    """
    file_url = request.base_url
    file_ur = file_url.split('index')[0]
    fellow = '{}model_files'.format(file_ur)
    params = OrderedDict([('date', date), ('model', model)])
    response = requests.get(fellow, params=urlencode(params))
    files = response.json()
    if len(files)==0:
        message = 'File not Found, we do not have drifter data for specified date'
        print('No drifter output for date specified')
        return_file = None
    else:
        return_file = files[0]
        message = None
    return return_file,message
        
@app.route('/model_files')
def api_date():
    """
    API request for local files in the model directory

    return: Json of all the files. (subset the file in the future?)
    """
    date = request.args.get('date')
    model = request.args.get('model')
    files = glob.glob(os.path.join('model_files/{}'.format(model), '*{}*'.format(date)))
    return jsonify(files)


@app.route('/ocean_models')
def ocean_models():
    """
    API request for ocean model names in the data directory

    return: Json of ocean model (CIOPSE, RIOPS etc).
    """
    model_list = os.listdir('model_files')
    fetch_meta = []
    for model in model_list:
        data_dir = 'model_files/{}'.format(model)
        model_files = os.listdir(data_dir)
        model_files.sort()
        duration = xr.open_dataset('{}/{}'.format(data_dir,model_files[0]))
        dataset_duration = len(duration.time.values)-1
        model_start_date = model_files[0].split("_")[2]
        model_end_date = model_files[-1].split("_")[2]
        datepicker_start_day = int(model_start_date[6:8]) + 1
        datepicker_end_day = int(model_end_date[6:8]) + 1
        reformat_sd = '{}-{}-{}'.format(model_start_date[:4], model_start_date[4:6], datepicker_start_day)
        reformat_ed = '{}-{}-{}'.format(model_end_date[:4], model_end_date[4:6], datepicker_end_day)
        time_list = []
        for hour in range(dataset_duration):
            hour *= 24
            if(hour > dataset_duration):
                continue
            time_list.append(hour)
        info = {
            "model" : model,
            "start_date" : reformat_sd,
            "end_date" : reformat_ed,
            "duration" : dataset_duration,
            "duration_list" : time_list[1:]}
        fetch_meta.append(info)
    return jsonify(fetch_meta)


LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max',
                                           'lon_min', 'lon_max'))
def plot_ocean_data(file_name, bbox, duration, etopo_file, output_file=None):
    """Plot the modelled ocean data
   
    Parameters
    ----------
    file_name : file
         file containing the computed trajecjory longitude and latitude 
        values.
    bbox : str or file-like object, optional
        Path to namelist bounding box to be used in plots.
    etopo_file : str or file-like object, optional
        Path to bathymetry file.
    output_file : str or file-like object, optional
        File to save ocean data plots to. If not provided, no plot file is saved.
    plot_style : str 
        mpl style to be applied to plots
        
    """
    dsa = xr.open_dataset(file_name)
    user_duration = dsa.time.values[int(duration)]
    user_duration = np.datetime_as_string(user_duration, unit='h')
    plt.style.use('seaborn-talk')
    fig, ax = plt.subplots(1,1,figsize=(20,15))
    cmap = plt.cm.get_cmap('copper')
    ds = dsa.sel(time=slice(user_duration))
    name = ds.mod_run_name.split('_')
    run_name = '{}_{}_{}_{}H'.format(name[0],
                                     name[1],
                                     name[2],
                                     str(duration).zfill(3))
    if int(duration) == 12:
        plt.title('Drift map of {}: duration = {} Hours'.format
                (run_name, duration))
    else:
        plt.title('Drift map of {}: duration = {} Day(s)'.format
                 (run_name, int(int(duration)/24)))

    # constants for bounding box area extension
    buffer_lon = 1.50
    buffer_lat = 1.00
    buffer_lat_max = 1.0
    buffer_lon_min = 2.10
    
    
    if bbox is not None:
        initial_bbox = bbox.split(" ")
        bbox_lon_min=float(initial_bbox[0])
        bbox_lat_min=float(initial_bbox[1])
        bbox_lon_max=float(initial_bbox[2])
        bbox_lat_max=float(initial_bbox[3])
        
        bbox_lat_min = bbox_lat_min - buffer_lat
        bbox_lon_min = bbox_lon_min - buffer_lon_min
        bbox_lat_max = bbox_lat_max + buffer_lat_max
        bbox_lon_max = bbox_lon_max + buffer_lon
        
        initial_bbox = LatLonBoundingBox(lon_min=bbox_lon_min,
                                         lat_min=bbox_lat_min, 
                                         lon_max=bbox_lon_max,
                                         lat_max=bbox_lat_max)
    else:
        print(("No inputs recorded in initial_bbox. "
                     "Initializing default particle grid "))
        initial_bbox = LatLonBoundingBox(lon_min = ds.mod_lon.min()-2, lat_min = ds.mod_lat.min()-2, 
                                         lon_max = ds.mod_lon.max()+2, lat_max = ds.mod_lon.min()+2)
   
    dlat = initial_bbox.lat_max - initial_bbox.lat_min
    dlon = initial_bbox.lon_max - initial_bbox.lon_min
    
    #basemap_plot=Basemap(projection='merc', resolution='l', llcrnrlon=initial_bbox.lon_min, 
    #                     llcrnrlat=initial_bbox.lat_min, urcrnrlon=initial_bbox.lon_max, 
    #                     urcrnrlat=initial_bbox.lat_max)
    
    basemap_settings = dict(
        llcrnrlon=initial_bbox.lon_min, llcrnrlat=initial_bbox.lat_min,
        urcrnrlon=initial_bbox.lon_max, urcrnrlat=initial_bbox.lat_max,
        ellps='WGS84', resolution='l', projection='merc', lat_ts=20.)
    if 'DRIFTWEB_CACHE_DIR' in os.environ:
        cache_file = joinpath(
            os.environ['DRIFTWEB_CACHE_DIR'],
            _get_basemap_cache_filename(basemap_settings))
    else:
        cache_file = None
    if cache_file is not None and pathexists(cache_file):
        with open(cache_file, 'rb') as f:
            basemap_plot = pickle.load(f)
    else:
        # Parameters for Mercator (merc) projection:
        # lat_ts = latitude of true scale
        basemap_plot = Basemap(**basemap_settings)
        if cache_file is not None and not pathexists(cache_file):
            os.makedirs(os.environ['DRIFTUTILS_CACHE_DIR'], exist_ok=True)
            with open(cache_file, 'wb') as f:
                pickle.dump(basemap_plot, f)

    if etopo_file is not None:
        plot_bathymetry(etopo_file,basemap_plot, initial_bbox)

    # Calculate maximum value of color bar
    dispmax = np.nanmax(ds.mod_disp.values)
    if dispmax/1000. > 400:
        vmax = 400
        vmin = 50
    else:
        vmax = round_up_base10(dispmax/1000.)
        vmin = 0

    def traj_thread(traj_value_x, traj_value_y, basemap_plot):
        basemap_plot.plot(traj_value_x, traj_value_y,
                          '-', linewidth = 0.4, color= 'k')
        mesh = basemap_plot.scatter(traj_value_x, traj_value_y,
                                    c=ds.mod_disp[md].values/1000,
                                    cmap=cmap, marker='o', s=6,
                                    vmin=vmin, vmax=vmax)
        basemap_plot.plot(traj_value_x[0], traj_value_y[0],
                          '.', color='k', markersize = 3)
        basemap_plot.plot(traj_value_x[-1], traj_value_y[-1],
                          '.', color='sandybrown', markersize = 3)
        return mesh   

    for md in range(0, len(ds.model_run)):
        traj_value_x, traj_value_y = basemap_plot(ds.mod_lon[md].values,
                                                  ds.mod_lat[md].values)
        mesh = traj_thread(traj_value_x, traj_value_y, basemap_plot)
        if md == 0:
            c_bar =basemap_plot.colorbar(mesh,location='bottom',pad="8%")
            c_bar.set_label('Displacement [km]')
    basemap_plot.drawmapboundary(fill_color='#A6CAE0', linewidth=0)
    
    # draw coastlines, country boundaries, fill continents.
    basemap_plot.fillcontinents(color='lightgrey', alpha=0.7,
                                lake_color='grey')
    basemap_plot.drawcoastlines(linewidth=0.8, color="black")
    
    # inputs for scale (km) on the map
    scale_lon_min = initial_bbox.lon_max - 3.8
    scale_lon_max = initial_bbox.lon_max - 2.5
    scale_lat_min = initial_bbox.lat_max - 1.8
    scale_lat_max = initial_bbox.lat_max - 1.20
    length = round(ds.mod_disp.values.max()//1000, -3)
    basemap_plot.drawmapscale(scale_lon_min,
                              scale_lat_min,
                              scale_lon_max,
                              scale_lat_max, 
                              vmax, fontsize=12)
        
    # parallels and meridiands for the maps
    meridian_dlat = dlat/3
    meridian_dlon = dlon/3
    parallels = np.arange(initial_bbox.lat_min,
                          initial_bbox.lat_max,
                          meridian_dlat)
    basemap_plot.drawparallels(parallels, labels=[1, 0, 0, 0],
                               fontsize=14, color='0.5', 
                               linewidth=0.5, fmt='%0.2f')
    meridians = np.arange(initial_bbox.lon_min,
                          initial_bbox.lon_max,
                          meridian_dlon)
    basemap_plot.drawmeridians(meridians,
                               labels=[0, 0, 0, 1],
                               fontsize = 14,
                               color='0.5', 
                               linewidth=0.5, fmt='%0.2f')
    ds.close()
    if output_file is None:
        output_file = 'static/{}.png'.format(run_name)
    fig.savefig(output_file, bbox_inches='tight', dpi=300, 
                papertype='letter', orientation='portrait')
    plt.close(fig)
    return output_file

def plot_bathymetry(bathymetry_file, basemap, bbox):
    """Plot topography/bathymetry on the given basemap.

    Parameters
    ----------
    bathymetry_file : str
        Name of netCDF containing bathymetry data.
    basemap : Basemap
        Basemap on which to plot the bathymetry contours.
    bbox : LatLonBoundingBox
        Bounding box for plot in terms of lat/lon.
    """

    # Read in ETOPO1 topography/bathymetry.
    # http://www.ngdc.noaa.gov/mgg/global/relief/ETOPO1/data/ice_surface/grid_registered/netcdf/ETOPO1_Ice_g_gmt4.grd.gz
    if 'DRIFTUTILS_CACHE_DIR' in os.environ:
        cache_file = joinpath(
            os.environ['DRIFTUTILS_CACHE_DIR'],
            get_bathymetry_cache_filename(bathymetry_file, basemap))
    else:
        cache_file = None
    if cache_file is not None and pathexists(cache_file):
        with open(cache_file, 'rb') as f:
            cache = pickle.load(f)
        x = cache['x']
        y = cache['y']
        bathy = cache['bathy']
    else:
        etopo = xr.open_dataset(bathymetry_file)
        lons = etopo['x'].values[:]
        lats = etopo['y'].values[:]

        res = utils.find_subset_indices(
            bbox.lat_min - 5, bbox.lat_max + 5,
            bbox.lon_min - 20, bbox.lon_max + 10, lats, lons)

        lon, lat = np.meshgrid(lons[res.lon_min: res.lon_max],
                               lats[res.lat_min: res.lat_max])
        x, y = basemap(lon, lat)

        bathy = etopo['z'][res.lat_min: res.lat_max,
                           res.lon_min: res.lon_max]

    levels = [-16000,-14000,-12000,-11000,-10000, -9000, -8000, -6000, -5000,
              -3000, -2000, -1500, -1000, 
              -500, -400, -300, -250, -200, -150, -100, -75,
              -65, -50, -35, -25, -15]
    #levels = [-2000, -1500, -1000, -500, -400, -300, -250, -200, -150, -100, -75, -65, -50, 
    #          -35, -25, -15, -10, -5, 0]
    contour_set = basemap.contourf(
        x, y, bathy, levels,
        cmap=LevelColormap(levels, cmap=cm.Blues_r),
        extend='neither', alpha=1.0, origin='lower'
    )
    contour_set.axis = 'tight'

    #commented out because we don't need the contours in the plot
    #basemap.contour(x, y, bathy, levels=[-500, -250], extend='neither',
    #                linewidths=0.05, linestyles='solid', colors='k')

    colorbar = plt.colorbar(contour_set,
                            label='Depth (m)')
    for t in colorbar.ax.get_yticklabels():
        t.set_fontsize(9)

    if cache_file is not None and not pathexists(cache_file):
        os.makedirs(os.environ['DRIFTUTILS_CACHE_DIR'], exist_ok=True)
        with open(cache_file, 'wb') as f:
            pickle.dump({'x': x, 'y': y, 'bathy': bathy}, f)

def round_up_base10(number):
    """ Routine for clean rounding up based on powers of 10
        eg. 1475 rounds to 2000
        15 rounds to 20
        0.01343453 rounds to 0.02

        arg number: float
            number to be rounded
    """
    if number != 0:
        power = np.floor(np.log10(np.abs(number)))
        result = 10**power*np.ceil(number/10**power)
        if number < 0:
           result = -result
    else:
        result = 0
    return result

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)